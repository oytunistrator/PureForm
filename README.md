<h1>Pure Form</h1>

<h5>Custom Form Plugin ( Select, Checkbox, Radio Button and File Upload )</h5>

<h4> Using </h4>
$("#elementName").pureForm();


<h4> Browser Support </h4>
* Internet Explorer 8+
* Google Chrome
* Firefox
* Safari
* Opera 
* All Mobile Browsers

<h2>v1.1 Out Now</h2>
Text Input, Numeric Input and Alphabetic Input added.
